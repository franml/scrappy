class AllMyVideos():
	def __init__(self, url):
		self.url = url

	def GetVideoFile(self):
		from functions import Request, NewSearch, GetFormDict, ReplaceChars

		headers, html = Request(self.url, "GET")
		form_dict = GetFormDict(html)
		
		headers, html = Request(self.url, "POST", None, form_dict["data"])

		return ReplaceChars(NewSearch(html, '"file" : "http://', len('"file" : "'), "\n", None)) 