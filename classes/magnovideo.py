class Magnovideo():
	def __init__(self, url):
		self.url = url
		
	def GetVideoFile(self):
		from functions import Request, NewSearch, GetCookies

		headers, html = Request(self.url, "GET")
		new_headers = GetCookies(html, "jQuery.cookie(")
		headers, html = Request(self.url, "GET", new_headers, None)
		
		return NewSearch(html, "http://o1.magnovideo.com/", 0, ".mp4", None)

