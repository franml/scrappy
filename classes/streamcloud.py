class StreamCloud():
	def __init__(self, url):
		self.url = url

	def GetVideoFile(self):
		from functions import Request, NewSearch, GetFormDict, ReplaceChars
		from time import sleep

		headers, html = Request(self.url, "GET")
		form_dict = GetFormDict(html)
		sleep(11)

		headers, html = Request(self.url, "POST", None, form_dict["data"])

		return ReplaceChars(NewSearch(html, "file: \"http://", len("file: \""), "\n", None)) 