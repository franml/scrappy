class PlayedTo():
	def __init__(self, url):
		self.url = url

	def GetVideoFile(self):
		from functions import Request, NewSearch, GetFormDict, ReplaceChars, GetCookies
		from time import sleep

		headers, html = Request(self.url, "GET")
		form_dict = GetFormDict(html)
		sleep(3)

		new_headers = GetCookies(html, "$.cookie")
		headers, html = Request(self.url, "POST", new_headers, form_dict["data"])

		return ReplaceChars(NewSearch(html, "file: \"http://", len("file: \""), "\n", None)) 