class MovShare():
	def __init__(self, url):
		self.url = url
		
	def GetVideoFile(self):
		from functions import Request, NewSearch, ReplaceChars
		from urllib import urlencode

		headers, html = Request(self.url, "GET")
		flashvars = NewSearch(html, "var flashvars = {};", None, "flashvars.premiumLink", 0).replace("\n", "").replace("\t", "").replace("flashvars.", "").split(";")

		var_dict = {}
		necesary_vars = ["cid", "file", "filekey"]
		for i in range(0, len(flashvars)):			
			for j in range(0, len(necesary_vars)): 
				if necesary_vars[j] in flashvars[i]: 
					var = flashvars[i].split("=")
					if var[0] == "filekey":
						var[0] = "key"
					var_dict[var[0]] = ReplaceChars(var[1])

		params = urlencode(var_dict)
		new_url = "http://www.movshare.net/api/player.api.php?"+params
		headers, html = Request(new_url, "GET")
		
		return NewSearch(html, "url=", None, ".flv", None)