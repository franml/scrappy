class DaClips():
	def __init__(self, url):
		self.url = url

	def GetVideoFile(self):
		from functions import Request, NewSearch, GetFormDict
		from time import sleep

		headers, html = Request(self.url, "GET")

		form_dict = GetFormDict(html, "<form method=\"post\" action=''>")
		del form_dict["data"]["method_premium"]

		sleep(6)
		headers, html = Request(self.url, "POST", None, form_dict["data"])
		
		return NewSearch(html, "file: \"", None, "\"", 0) 