class Vk():
	def __init__(self, url):
		self.url = url

	def GetVideoFile(self):
		from functions import Request, NewSearch
		from ast import literal_eval
		
		a = []
		headers, html = Request(self.url, "GET")
		vk_dict = NewSearch(html, "var vars = ", None, "\n", None)
		if not vk_dict:
			return "ERROR7"
		vk_dict = literal_eval(vk_dict)

		for i in range(0, len(vk_dict.keys())):
			if 'url' in vk_dict.keys()[i]:
				resolution = vk_dict.keys()[i][3:]
				if len(resolution) == 3:
					a.append(resolution)

		return vk_dict["url"+str(max(a))]