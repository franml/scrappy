def Request(url, method, headers = None, body = None):
	from httplib2 import Http
	from urllib import urlencode
	from sys import exit 
	import socket 

	socket.setdefaulttimeout(3)
	
	if headers is None:
		headers = {}

	allheaders = { "User-Agent" : "Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.132 Safari/537.36" }
	
	if method == "POST":
		allheaders.update ({ "Content-type": "application/x-www-form-urlencoded" })
	
	allheaders.update(headers)
		
	try:
		http_instance = Http(".cache")
		if body != None:
			body = urlencode(body)
		ret = ()
		ret = http_instance.request(url, method, headers = allheaders, body = body)
		print len(ret)
		return ret
	except socket.error:
		print "ERROR3"
		exit() 
	except Exception:
		print "ERROR4"
		exit()

def NewSearch(text, ini_value, ini_value_len, end_value, end_value_len): # HELPER FUNCTION
	if ini_value_len == None:
		ini_value_len = len(ini_value)		
	if end_value_len == None:
		end_value_len = len(end_value)

	ini = text.find(ini_value) 
	if ini == -1:
		return False
	else:
		ini += ini_value_len
		end = text.index(end_value, ini) + end_value_len
		
	return text[ini:end]

def ReplaceChars(a):  # HELPER FUNCTION
	
	return a.replace("\r","").replace("\"","").replace(">","").replace("'","").replace(",", "").replace(" ", "").replace(";", "").replace(")", "").replace("(", "")

def GetFormDict(html, tocacojones = None):
	if tocacojones == None:
		tocacojones = "<form"

	form = NewSearch(html.lower(), tocacojones, 0, "</form", None)
	if not form:
		return False 
	
	form = form.split(">")

	form_dict = {}
	form_dict_data = {}

	form_0 = form.pop(0).split(" ")
	a = False
	b = False
	
	for i in range(0, len(form_0)):
		if 'name' in form_0[i]:
			form_dict['name'] = ReplaceChars(form_0[i][len('name='):])

		elif 'action' in form_0[i]:
			form_dict['action'] = ReplaceChars(form_0[i][len('action='):])

	for i in range(0, len(form)):
		if not '<input' in form[i].lower():
			form[i] = ''

		elif 'name=' in form[i]:
			formi = form[i].split("\"")

			for k in range(0, len(formi)):
				if 'name=' in formi[k].lower():
					name = ReplaceChars(formi[k+1])
					a = True

				elif 'value=' in formi[k].lower():
					value = ReplaceChars(formi[k+1])
					b = True

				if a and b:
					form_dict_data[name] = value

	form_dict['data'] = form_dict_data
	form_dict['data']['imhuman'] = "Ver el v%C3%ADdeo ahora"   #eso no es bien
	return form_dict

def GetCookies(html, cookie_name):
	cookie = NewSearch(html, cookie_name, None, ");", None)
	if not cookie:
		return "ERROR8"
	else:
		cookie = cookie.split(",")
		return { 'Cookie': ReplaceChars(cookie[0])+"="+ReplaceChars(cookie[1])+";" }