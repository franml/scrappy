def getFile(url):
	if 'streamcloud' in url:
		from classes import streamcloud
		video = streamcloud.StreamCloud(url)
	
	elif 'played.to' in url:
		from classes import playedto
		video = playedto.PlayedTo(url)
	
	elif 'vidspot' in url: # allmyvideos.py = vidspot.py
		from classes import vidspot
		video = vidspot.Vidspot(url)
		
	elif 'allmyvideos' in url: # allmyvideos.py = vidspot.py
		from classes import allmyvideos
		video = allmyvideos.AllMyVideos(url)

	elif 'vk' in url:
		from classes import vk
		video = vk.Vk(url)

	elif 'magnovideo' in url:
		from classes import magnovideo
		video = magnovideo.Magnovideo(url)
	
	elif 'nowvideo' in url: # nowvideo.py ~ movshare.py = novamov.py
		from classes import nowvideo
		video = nowvideo.NowVideo(url)

	elif 'streamin.to' in url:
		return "BlackList"

	elif 'movshare' in url: # movshare.py = novamov.py ~ nowvideo.py
		from classes import movshare
		video = movshare.MovShare(url)

	elif 'novamov' in url: # movshare.py = novamov.py ~ nowvideo.py
		from classes import novamov
		video = novamov.NovaMov(url)

	elif 'moevideos' in url:
		return "ERROR1"
		from classes import moevideos
		video = moevideos.MoeVideos(url)

	elif 'daclips' in url:
		from classes import daclips
		video = daclips.DaClips(url)

	else:
		return "ERROR1"

	return video.GetVideoFile()

'''
PROVA TOTES URLs:
'''
'''

urls = ["http://www.vidspot.net/n1mtx4z2ji23", 
		"http://www.magnovideo.com/?v=AIZ4A4GR",
		"http://vk.com/video_ext.php?oid=181776299&id=163236243&hash=f4cc0fab64bc4463&hd=1", 
		"http://streamin.to/eapesc545lav",
		"http://www.novamov.com/video/yi2n4tehvqmcg",
		"http://www.movshare.net/video/c6ilujh1912sy",
		"http://allmyvideos.net/w5ykulqaxvob",
		"http://played.to/9zs344gfqe9b", 
		"http://daclips.in/4czaqyoheq9d",
		"http://streamcloud.eu/qh59v6z8zn9y"]


for i in range(0, len(urls)):
	print getFile(urls[i])+"\n\n"

'''
url = "http://www.nowvideo.sx/video/u2gr2wxcwfo1l"
print getFile(url)